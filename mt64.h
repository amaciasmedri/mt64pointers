/****************************************************** **
 ** Mersenne Twister 64-bit version with pointers.      **
 ** This version reproduces the same values but it      **
 ** is significantly faster than the original algorithm **
 ** *************************************************** **/

#define mt64_NN 312
#define mt64_UM 0xFFFFFFFF80000000ULL       // Most significant 33 bits
#define mt64_LM 0x7FFFFFFFULL               // Least significant 31 bits
#define mt64_MAX 18446744073709551615ULL

static const unsigned long long mt64_mag01[2]={0ULL,0xB5026F5AA96619E9ULL};

// Vectors of pointers (*mt64_tv1 and *mt64_tv2) were added in this version
static unsigned long long mt64_mt[mt64_NN],*mt64_tv1[mt64_NN],*mt64_tv2[mt64_NN]; 

static int mt64_mti=0;

// initializes mt64_mt[NN] with a seed
void mt64_init_genrand64(unsigned long long seed) {
    const unsigned long long SM1=6364136223846793005ULL;
    const int MM=mt64_NN/2;
    mt64_mt[0]=seed;
    int i;
    for(i=1;i<mt64_NN;i++) mt64_mt[i]=SM1*(mt64_mt[i-1]^(mt64_mt[i-1]>>62))+(unsigned long long)i;
    
    // mt64_tv1[i] is the pointer to the next position of mt64_mt[i] with periodic condition
    for(i=0;i<mt64_NN-1;i++) mt64_tv1[i]=&mt64_mt[i+1];
    mt64_tv1[i]=&mt64_mt[0];
    
    // mt64_tv2 is a shuffle of mt64_mt defined by Makoto Matsumoto
    for(i=0;i<mt64_NN-MM;i++) mt64_tv2[i]=&mt64_mt[i+MM];
    for(;i<mt64_NN-1;i++) mt64_tv2[i]=&mt64_mt[i-(mt64_NN-MM)];
    mt64_tv2[i]=&mt64_mt[MM-1];
}

// initialize by an array with array-length
void mt64_init_by_array64(unsigned long long init_key[],int key_length) {
    const unsigned long long SM2=3935559000370003845ULL;
    const unsigned long long SM3=2862933555777941757ULL;        
    int i=1,j=0,k;
    
    // A general seed R is computed as an average of init_key[]
    unsigned long long R=0ULL;
    for(k=0;k<key_length;k++) R+=init_key[k]/(unsigned long long)key_length;
    mt64_init_genrand64(mt64_MAX-R);
    
    for(k=mt64_NN>key_length?mt64_NN:key_length;k;k--) {
        mt64_mt[i]=(mt64_mt[i]^((mt64_mt[i-1]^(mt64_mt[i-1]>>62))*SM2))+init_key[j]+(unsigned long long)j;
        i++; j++;
        if(i>=mt64_NN) { mt64_mt[0]=mt64_mt[mt64_NN-1]; i=1; }
        if(j>=key_length) j=0;
    }
    for(k=mt64_NN-1;k;k--) {
        mt64_mt[i]=(mt64_mt[i]^((mt64_mt[i-1]^(mt64_mt[i-1]>>62))*SM3))-(unsigned long long)i;
        i++;
        if(i>=mt64_NN) { mt64_mt[0]=mt64_mt[mt64_NN-1]; i=1; }
    }
    mt64_mt[0]=1ULL<<63;
}

// It generates a random number on [0, 2^64-1]-interval
unsigned long long mt64_genrand64_int64(void) {
    
    // The following line can be omitted if mt64_NN=256 and mt64_mti is an "unsigned char" variable type
    if(mt64_mti==mt64_NN) mt64_mti=0;
    
    // The element mt64_mt[mt64_mti] (and only this) is generated when mt64_genrand64_int64 is invoked
    // This is possible because the vector elements (*mt64_tv1[i] and *mt64_tv2[i]) are consecutive
    unsigned long long x=(mt64_mt[mt64_mti]&mt64_UM)|(*mt64_tv1[mt64_mti]&mt64_LM);
    mt64_mt[mt64_mti]=*mt64_tv2[mt64_mti]^(x>>1)^mt64_mag01[x%2ULL];
    
    x=mt64_mt[mt64_mti++];
    x^=(x>>29)&0x5555555555555555ULL;
    x^=(x<<17)&0x71D67FFFEDA60000ULL;
    x^=(x<<37)&0xFFF7EEE000000000ULL;
    return x^(x>>43);

    // So, in the algorithm, it is not necessary to use:
    // (1) loops "for", and (2) the sums "i+1" and "i+(MM-NN)" to get the elements of mt64_mt
}

inline long long mt64_genrand64_int63(void) { return(long long)(mt64_genrand64_int64()>>1); } // [0,2^63-1]
inline double mt64_genrand64_real1(void) { return(mt64_genrand64_int64()>>11)*(1.0/9007199254740991.0); } // [0,1]
inline double mt64_genrand64_real2(void) { return(mt64_genrand64_int64()>>11)*(1.0/9007199254740992.0); } // [0,1)
inline double mt64_genrand64_real3(void) { return((mt64_genrand64_int64()>>12)+0.5)*(1.0/4503599627370496.0); } // (0,1)

inline int mt64_Round(int M) { return(int)(mt64_genrand64_real2()*(double)M); } // { 0,1,2,...,M-1 }
#define mt64_Discrete(n1,n2) (n1+mt64_Round(n2-n1+1))